package com.example.viniciusns.atividade_explicita2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Atividade_1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atividade_1);

        Button bt = (Button) findViewById(R.id.bt_para_at2);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intente = new Intent(Atividade_1.this, Atividade_2.class);
                startActivity(intente);
            }
        });
    }
}
