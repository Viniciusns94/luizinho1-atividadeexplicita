package com.example.viniciusns.atividade_explicita2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Atividade_2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atividade_2);

        Button bt2 = (Button) findViewById(R.id.bt_para_at3);
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(Atividade_2.this, Atividade_3.class);
               startActivity(intent);
            }
        });
    }
}
